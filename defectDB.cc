#include "defectDB.h"

defectDB::defectDB(string _filePath){
	m_filePath = _filePath;
}

void defectDB::buildDB(){
	cout<<"start building DB...";
	char line[bufSize],name[bufSize/2];
	fstream fp;
	string file = m_filePath + "defectDB";
	fp.open(file.c_str(),ios::in);
	if(!fp){
		cout<<"Fail to open the defectDB file "<<m_filePath<<" abort!"<<endl;
		exit(1);
	}
	defectTable *table;
	while(fp.getline(line,sizeof(line),'\n')){
		table = new defectTable(m_filePath + string(line));
		sscanf(line,"%*[^.].%s",name);
		m_defectTableMap.insert(
				map<string,defectTable*>::value_type(
					string(name),table));
	}
	cout<<"done"<<endl;
}

defectTable* defectDB::getDefectTableByName(string name){
	map<string, defectTable*>::iterator it;
	it = m_defectTableMap.find(name);
	if(it == m_defectTableMap.end()) return NULL;
	return it->second;

}
