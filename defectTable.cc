#include "defectTable.h"

defectTable::defectTable(string _filePath){
	m_filePath = _filePath;
	parse();
}
/*
defectTable::defectTable(defectTable *table){
	m_filePath = table->getFilePath();
	for(int i = 0;i < table->getDefectListSize();i++){
		defect newDefect(table->getDefect(i));
        m_defectList.push_back(newDefect);
	}
	UDT_STR_VEC_MAP::iterator iter;

}
*/

void defectTable::parse(){
  fixCondition();
  char line[bufSize*2];
  int PHASE;
  fstream fp;
  fp.open(m_filePath.c_str(),ios::in);
  if(!fp){
    cout<<"Fail to open defect file "<<m_filePath<<endl<<" abort"<<endl;
    exit(1);
  }
  while(fp.getline(line,sizeof(line),'\n')){
	if(strstr(line,"Finish building defect table") != NULL) PHASE = BUILD_DEFECT_LIST;
	else if(strstr(line,"Condition") != NULL) PHASE = BUILD_DEFECT_TABLE;
	else PHASE = STALL;
	
	switch(PHASE){
		case BUILD_DEFECT_LIST:
			buildDefectList(fp);
			break;
		case BUILD_DEFECT_TABLE:
			buildDefectTable(line);
			break;
		default:
			break;
	}

  }
  //Fill up the zeros
  fp.close();
  //cout<<m_filePath<<" done "<<endl;
}

//TODO remove this if the logs are fixed
void defectTable::fixCondition(){
  char line[bufSize*2],condition[16];;
  int PHASE;
  fstream fp;
  fp.open(m_filePath.c_str(),ios::in);
  if(!fp){
    cout<<"Fail to open defect file "<<m_filePath<<endl<<" abort"<<endl;
    exit(1);
  }
  while(fp.getline(line,sizeof(line),'\n')){
    if(strstr(line,"Finish building defect table") != NULL) PHASE = BUILD_DEFECT_LIST;
    else if(strstr(line,"Condition") != NULL) PHASE = BUILD_DEFECT_TABLE;
    else PHASE = STALL;

    switch(PHASE){
        case BUILD_DEFECT_LIST:
            break;                                                                                                                                                                   
        case BUILD_DEFECT_TABLE:                                                          
		    sscanf(line,"%*[^ ] %s",condition);
			if(m_inputNum < strlen(condition)) m_inputNum = strlen(condition);
            break;                                                                        
        default:
            break;                                                                        
    }
                                                                                          
  }

}

//Build the defect lint for the defect table map
//TODO refine this
void defectTable::buildDefectList(fstream &fp){
	char line[bufSize*2];
	bool end = true;
	vector<string > nameList;
	string name;

	//read the name list
	while(fp.getline(line,sizeof(line),'\n')){
		end = true;
		for(int i = 0;i < strlen(line);i++){
			if(line[i] != ' ') end = false;
		}
		nameList.push_back(string(line));
		if(end) break;;
	}
	for(int i = 0;i < nameList[0].size();i++){
		if(nameList[0][i] == ' ') continue;
		name = "";
		for(int j = 0; j < nameList.size();j++){
			name += nameList[j][i];
		}
		defect newDefect(name,BRIDGE);
		m_defectList.push_back(newDefect);
	}		
}


void defectTable::buildDefectTable(char *line){
	char condition[bufSize/4];
	int j;
	sscanf(line,"%*[^ ] %s",condition);
	vector<int > defectTable;
	for(int i = 0;i < m_defectList.size();i++){
		j = 2*i;
		if(line[j] == '0' || line[j] == '1'){
			defectTable.push_back(i);
		}
	}	
	//TODO remove if logs are fixed
	string condition_fix;
	condition_fix = string(condition);
//	cout<<m_filePath<<" "<<m_inputNum<<endl;
	for(int i = condition_fix.size();i < m_inputNum;i++)
		condition_fix += "0";
	//cout<<condition_fix<<endl;

	m_defectTable.insert(UDT_STR_INT_MAP::value_type(condition_fix,defectTable));
}

int defectTable::countDetected(){
	int num = 0;
	for(int i = 0;i < m_defectList.size();i++){
		if(m_defectList[i].isDetected()){
			num++;
		}
	}

	return num;
}

void defectTable::setDetected(string inputPattern){
//cout<<m_filePath<<" "<<inputPattern<<endl;
	UDT_STR_INT_MAP::iterator iter;
	iter = m_defectTable.find(inputPattern);
	if(iter == m_defectTable.end()){
		cout<<"ERROR"<<endl;
	}
	for(int i = 0;i < iter->second.size();i++){
		m_defectList.at(iter->second[i]).setDetected();
	}
}

void defectTable::setAllUndetected(){
	for(int i = 0;i < m_defectList.size();i++){
		m_defectList[i].setUndetected();
	}
}

int defectTable::countAllPossibleDefect(){
	UDT_STR_INT_MAP::iterator iter;
	for(iter = m_defectTable.begin();iter != m_defectTable.end();iter++){
		for(int i = 0;i < iter->second.size();i++)
			m_defectList[ iter->second[i] ].setDetected();
	}
	int total = countDetected();
	setAllUndetected();
	return total;	
}

