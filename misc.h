#ifndef MISC_H
#define MISC_H

#include <algorithm>
#include <exception>
#include <string>
#include <sstream>
#include <stdio.h>
#include <vector>

using namespace std;

class expObj:public exception{
  public:
    expObj(const char *err):m_msg(err){};
    virtual const char* what() const throw(){return m_msg;}
  private:
    const char* m_msg;
};


template <class T> 
string toString(const T &t){
	ostringstream os;
	os<<t;
	return os.str();
}


inline string dec2bin(int n){
  string res;
  while(n){
    res.push_back((n & 1) + '0');
    n >>= 1;
  }
  if(res.empty())
    res = "0";
  else
    reverse(res.begin(),res.end());
  return res;
};


//for better performance
inline void toLowerPtr(char *p);
inline void toLower(string &in){
	char *c = const_cast<char *>(in.c_str());
	size_t l = in.size();
	for(char *c2 = c;c2 < c + l;c2++)
		toLowerPtr(c2);
};

inline void toLowerPtr(char *p){
	switch(*p){
		case 'A':*p = 'a';return;
    case 'B':*p = 'b';return;
    case 'C':*p = 'c';return;
    case 'D':*p = 'd';return;
    case 'E':*p = 'e';return;
    case 'F':*p = 'f';return;
    case 'G':*p = 'g';return;
    case 'H':*p = 'h';return;
    case 'I':*p = 'i';return;
    case 'J':*p = 'j';return;
    case 'K':*p = 'k';return;
    case 'L':*p = 'l';return;
    case 'M':*p = 'm';return;
    case 'N':*p = 'n';return;
    case 'O':*p = 'o';return;
    case 'P':*p = 'p';return;
    case 'Q':*p = 'q';return;
    case 'R':*p = 'r';return;
    case 'S':*p = 's';return;
    case 'T':*p = 't';return;
    case 'U':*p = 'u';return;
    case 'V':*p = 'v';return;
    case 'W':*p = 'w';return;
    case 'X':*p = 'x';return;
    case 'Y':*p = 'y';return;
    case 'Z':*p = 'z';return;
	}
};
//end of toLower


#endif
