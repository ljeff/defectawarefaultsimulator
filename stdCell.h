#ifndef __STD_CELL_H__
#define __STD_CELL_H__

#include <iostream>
#include "defectTable.h"
#include "misc.h"
//#include "circuit.h"
#include "gate.h"

class stdCell{
	public:
		stdCell(){};
		stdCell(char *line);
		void parseInput(char *line);
		void parseOutput(char *line);
//		void connectInput(CIRCUIT &);
//		void connectOutput(CIRCUIT &);
		void setDefectTable(defectTable *table);
		string getName(){return m_name;}
		string getType(){return m_type;}
		int getInputNum(){return m_inputList.size();}
		int getOutputNum(){return m_outputList.size();}
		GATE* getInputGate(int i){return m_input[i];}
		GATE* getOutputGate(int i){return m_output[i];}
		string getInputName(int i){return m_inputList[i];}
		string getOutputName(int i){return m_outputList[i];}
		void addInputGate(GATE *gate){m_input.push_back(gate);}
		void addOutputGate(GATE *gate){m_output.push_back(gate);}

		int allDefectNum(){return m_defectTable->countAllPossibleDefect();}
		int countDetectedDefect(){return m_defectTable->countDetected();}
		void setDefectDetected();

	private:
		defectTable *m_defectTable;
		string m_type;
		string m_name;
		vector<string> m_inputList;
		vector<string> m_outputList;
		vector<GATE *> m_input;
		vector<GATE *> m_output;

};


#endif
