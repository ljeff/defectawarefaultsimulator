#ifndef __DEFECT_DB__
#define __DEFECT_DB__

#include <map>
#include <iostream>
#include <fstream>
#include "misc.h"
#include "defectTable.h"

class defectDB{
	public:
		defectDB(){};
		defectDB(string _filePath = "../log/");
		void buildDB();
		defectTable* getDefectTableByName(string name);
	private:
		string m_filePath;
		map<string, defectTable*> m_defectTableMap;

};


#endif
	
