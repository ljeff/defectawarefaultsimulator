#include "stdCell.h"

stdCell::stdCell(char *line){
	char stdCellType[64],stdCellName[128];
	sscanf(line,"%*s %s %s",stdCellType,stdCellName);
	m_type = string(stdCellType);
	m_name = string(stdCellName);
}

void stdCell::parseInput(char *line){
	char *pch;
	int c = 0;
	int i = 0;

	pch = strtok(line," ");
	pch = strtok(NULL," ");
	c = atoi(pch);
	pch = strtok(NULL," ");

	while(pch != NULL){
		i++;
//		printf("%s ",pch);
		m_inputList.push_back(string(pch));
		pch = strtok(NULL," ");
	}
	string err = m_name + " stdCellMap input number miss match";
	if(i != c) throw expObj(err.c_str() );
}

void stdCell::parseOutput(char *line){
    char *pch;                                                                            
    int c = 0;
    int i = 0; 

    pch = strtok(line," ");                                                               
    pch = strtok(NULL," ");
    c = atoi(pch); 
	pch = strtok(NULL," ");

    while(pch != NULL){
        i++;
        //printf("%s ",pch);
        m_outputList.push_back(string(pch));
        pch = strtok(NULL," ");
    }
    string err = m_name + " stdCellMap output number miss match";
    if(i != c) throw expObj(err.c_str() );
}
/*
void stdCell::connectInput(CIRCUIT &circuit){
	GATE *gptr;
	for(int i = 0;i < m_inputList.size();i++){
		gptr = circuit.getGateByName(m_inputList[i]);
		if(gptr == NULL){
			string err = string("Cant find gate ") + m_inputList[i];
			throw expObj(err.c_str());
		}
		m_input.push_back(gptr);
		cout<<"input "<<gptr->GetName()<<" connected"<<endl;
	}	
}


void stdCell::connectOutput(CIRCUIT &circuit){
    GATE *gptr;
    for(int i = 0;i < m_outputList.size();i++){
        gptr = circuit.getGateByName(m_outputList[i]);
        if(gptr == NULL){
            string err = string("Cant find gate ") + m_outputList[i];
            throw expObj(err.c_str());
        }
        m_output.push_back(gptr);
        cout<<"output "<<gptr->GetName()<<" connected"<<endl;
    }   
}
*/
void stdCell::setDefectTable(defectTable *table){
	m_defectTable = new defectTable(*table);
}

void stdCell::setDefectDetected(){
	string input;
	for(int i = 0;i < m_input.size();i++){
	  //cout<<m_input[i]->GetName()<<endl;
		input += m_input[i]->getValueString();
	}
	if(input == "") return;
	m_defectTable->setDetected(input);
}




