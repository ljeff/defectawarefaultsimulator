#include <iostream> 
//#include <alg.h>
#include "circuit.h"
#include "GetLongOpt.h"
using namespace std;

extern GetLongOpt option;

void CIRCUIT::FanoutList()
{
    unsigned i = 0, j;
    GATE* gptr;
    for (;i < No_Gate();i++) {
        gptr = Gate(i);
        for (j = 0;j < gptr->No_Fanin();j++) {
            gptr->Fanin(j)->AddOutput_list(gptr);
        }
    }
}

void CIRCUIT::Levelize()
{
    list<GATE*> Queue;
    GATE* gptr;
    GATE* out;
    unsigned j = 0;
    for (unsigned i = 0;i < No_SI();i++) {
        gptr = SIGate(i);
        gptr->SetLevel(0);
        for (j = 0;j < gptr->No_Fanout();j++) {
            out = gptr->Fanout(j);
            if (out->GetFunction() != G_PPI) {
                out->IncCount();
                if (out->GetCount() == out->No_Fanin()) {
                    out->SetLevel(1);
                    Queue.push_back(out);
                }
            }
        }
    }
    for (unsigned i = 0;i < No_PI();i++) {
        gptr = PIGate(i);
        gptr->SetLevel(0);
        for (j = 0;j < gptr->No_Fanout();j++) {
            out = gptr->Fanout(j);
            if (out->GetFunction() != G_PPI) {
                out->IncCount();
                if (out->GetCount() == out->No_Fanin()) {
                    out->SetLevel(1);
                    Queue.push_back(out);
                }
            }
        }
    }
    for (unsigned i = 0;i < No_PPI();i++) {
        gptr = PPIGate(i);
        gptr->SetLevel(0);
        for (j = 0;j < gptr->No_Fanout();j++) {
            out = gptr->Fanout(j);
            if (out->GetFunction() != G_PPI) {
                out->IncCount();
                if (out->GetCount() ==
                        out->No_Fanin()) {
                    out->SetLevel(1);
                    Queue.push_back(out);
                }
            }
        }
    }
    int l1, l2;
    while (!Queue.empty()) {
        gptr = Queue.front();
        Queue.pop_front();
        l2 = gptr->GetLevel();
        for (j = 0;j < gptr->No_Fanout();j++) {
            out = gptr->Fanout(j);
            if (out->GetFunction() != G_PPI) {
                l1 = out->GetLevel();
                if (l1 <= l2)
                    out->SetLevel(l2 + 1);
                out->IncCount();
                if (out->GetCount() ==
                        out->No_Fanin()) {
                    Queue.push_back(out);
                }
            }
        }
    }
    for (unsigned i = 0;i < No_Gate();i++) {
        Gate(i)->ResetCount();
    }
}

void CIRCUIT::Check_Levelization()
{

    GATE* gptr;
    GATE* in;
    unsigned i, j;
    for (i = 0;i < No_Gate();i++) {
        gptr = Gate(i);
        if (gptr->GetFunction() == G_PI) {
            if (gptr->GetLevel() != 0) {
                cout << "Wrong Level for PI : " <<
                gptr->GetName() << endl;
                exit( -1);
            }
        }
        else if (gptr->GetFunction() == G_PPI) {
            if (gptr->GetLevel() != 0) {
                cout << "Wrong Level for PPI : " <<
                gptr->GetName() << endl;
                exit( -1);
            }
        }
        else {
            for (j = 0;j < gptr->No_Fanin();j++) {
                in = gptr->Fanin(j);
                if (in->GetLevel() >= gptr->GetLevel()) {
                    cout << "Wrong Level for: " <<
                    gptr->GetName() << '\t' <<
                    gptr->GetID() << '\t' <<
                    gptr->GetLevel() <<
                    " with fanin " <<
                    in->GetName() << '\t' <<
                    in->GetID() << '\t' <<
                    in->GetLevel() <<
                    endl;
                }
            }
        }
    }
}

void CIRCUIT::SetMaxLevel()
{
    for (unsigned i = 0;i < No_Gate();i++) {
        if (Gate(i)->GetLevel() > MaxLevel) {
            MaxLevel = Gate(i)->GetLevel();
        }
    }
}

//Setup the Gate ID and Inversion
//Setup the list of PI PPI PO PPO
void CIRCUIT::SetupIO_ID()
{
    unsigned i = 0;
    GATE* gptr;
    vector<GATE*>::iterator Circuit_ite = Netlist.begin();
    for (; Circuit_ite != Netlist.end();Circuit_ite++, i++) {
        gptr = (*Circuit_ite);
        gptr->SetID(i);
        switch (gptr->GetFunction()) {
			case G_TIE0:
			case G_TIE1:
			case G_TIEX:
				SIlist.push_back(gptr);
				break;
            case G_PI: PIlist.push_back(gptr);
                break;
            case G_PO: POlist.push_back(gptr);
                break;
            case G_PPI: PPIlist.push_back(gptr);
                break;
            case G_PPO: PPOlist.push_back(gptr);
                break;
            case G_NOT: gptr->SetInversion();
                break;
            case G_NAND: gptr->SetInversion();
                break;
            case G_NOR: gptr->SetInversion();
                break;
            default:
                break;
        }
    }
}

void CIRCUIT::buildGateMap(){
	for(int i = 0;i < Netlist.size();i++){
		m_gateMap.insert(
				UDT_STR_GATE_MAP::value_type(Netlist[i]->GetName(),Netlist[i]));
//cout<<Netlist[i]->GetName()<<endl;
	}

}

GATEPTR CIRCUIT::getGateByName(string name){
	UDT_STR_GATE_MAP::iterator it;
	it = m_gateMap.find(name);
	if(it == m_gateMap.end()) return NULL;
	else return it->second;	
}

void CIRCUIT::insertCAFault(){
	cout<<"Insert CA fault"<<endl;
    register unsigned i, j;
    GATEFUNC fun;
    GATEPTR gptr, fanout;
    FAULT *fptr;
    for (i = 0;i<m_realPO.size();i++) {
        gptr = m_realPO[i]; 
		fun = gptr->GetFunction();
        if (fun == G_PO) { continue; } //skip PO
        //add stem stuck-at 0 fault to Flist
        fptr = new FAULT(gptr, gptr, S0);
        Flist.push_front(fptr);
        //add stem stuck-at 1 fault to Flist
        fptr = new FAULT(gptr, gptr, S1);
        Flist.push_front(fptr);
//cout<<"Flist:"<<gptr->GetName()<<endl;
    } //end all gates
    //copy Flist to undetected Flist (for fault simulation)
    UFlist = Flist;
    return;

}

void CIRCUIT::connectStdCellMap(stdCellMap *cellMap){
	cout<<"Connecting stdCellMap...";
	vector<stdCell *> cellVector;
	cellMap->getStdCellVector(cellVector);
	stdCell *cell;
	for(int i = 0;i < cellVector.size();i++){
		cell = cellVector[i];
		for(int j = 0;j < cell->getInputNum();j++){
			cell->addInputGate(getGateByName(cell->getInputName(j)));
		}
		//cout<<cell->getName()<<" stdCell input connected "<<endl;
		for(int j = 0;j < cell->getOutputNum();j++){
			cell->addOutputGate(getGateByName(cell->getOutputName(j)));
		}
		//cout<<cell->getName()<<" stdCell output connected"<<endl;
	}
	cout<<"done"<<endl;
}

void CIRCUIT::setRealPO(stdCellMap *cellMap){
    vector<stdCell *> cellVector;                                                         
    cellMap->getStdCellVector(cellVector);                                                
    stdCell *cell;
    for(int i = 0;i < cellVector.size();i++){                                             
        cell = cellVector[i];
        for(int j = 0;j < cell->getOutputNum();j++){                                      
			m_realPO.push_back(cell->getOutputGate(j));
        }                                                                                 
		//cout<<"realPO "<<cell->getName()<<" identified"<<endl;
    }

}

