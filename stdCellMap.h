#ifndef __STD_CELL_MAP_H__
#define __STD_CELL_MAP_H__

#include <iostream>
#include <fstream>
#include "defectTable.h"
#include "stdCell.h"
#include "misc.h"
//#include "circuit.h"
#include "defectDB.h"

using namespace std;

typedef map<string, stdCell*> UDT_STR_CELL_MAP;

class stdCellMap{
	public:
		stdCellMap(string _mapFile);
		~stdCellMap(){};

		void parse();
//		void connectToCircuit(CIRCUIT &);
		void connectDB(defectDB *);
//		void setRealPO(CIRCUIT &);
		void getStdCellVector(vector<stdCell*> &vec);
		int allDefectNum();
		void setDefectDetected(string name);
		stdCell* getCellByName(string name);
		stdCell* getCellByOutputName(string name);
		int countDetectedDefect();

	private:
		UDT_STR_CELL_MAP m_stdCellMap;
		string m_filePath;

};


#endif

