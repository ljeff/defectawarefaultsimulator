#ifndef __DEFECT_H__
#define __DEFECT_H__

#include <iostream>
#include "typeemu.h"

using namespace std;

class defect{
	public:
		defect(string _name,DEFECT_TYPE _type)
			{m_name = _name;m_type = _type;m_detected = false;}
		defect(){};
		~defect(){};
		void setDetected(){m_detected = true;}
		void setUndetected(){m_detected = false;}
		bool isDetected(){return m_detected;}
		DEFECT_TYPE getType(){return m_type;}
		string getName(){return m_name;}
	
	private:
		string m_name;
		DEFECT_TYPE m_type;
		bool m_detected;

};

#endif
