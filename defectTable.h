#ifndef __DEFECT_TABLE_H__
#define __DEFECT_TABLE_H__

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "defect.h"

using namespace std;

typedef map<string, vector<int > > UDT_STR_INT_MAP;

class defectTable{
	public:
		defectTable(string _filePath);
		//defectTable(defectTable *);
		defectTable(){};
		~defectTable(){};
		void parse();
		void buildDefectList(fstream &);
		void buildDefectTable(char *);
		int countDetected();
//		int countUndetected(){m_defectList.size() - countDetected();}
		void setDetected(string inputPattern);
		void setAllUndetected();
		string getFilePath();
		int getDefectListSize(){return m_defectList.size();}
		defect *getDefect(int i){return &m_defectList[i];}
		int countAllPossibleDefect();		
		void fixCondition();

/*
		void dump(){cout<<"TT"<<m_filePath<<endl;}
		void setList(){m_defectList[9].setDetected();cout<<m_defectList[9].getName()<<endl;}
		void setMap(){m_defectTable.begin()->second[0]->setDetected();}
		void checkMap(){cout<<m_defectTable.begin()->second[0]->getName()<<":"<<m_defectTable.begin()->second[0]->isDetected()<<endl;}
		void checkList(){cout<<m_defectList[9].getName()<<":"<<m_defectList[9].isDetected()<<endl;}
		int num(){return m_defectList.size();}
		int num2(){return m_defectTable.begin()->second.size();}
	*/


	private:
		int m_inputNum;
		string m_filePath;
		//map<string, vector<defect *> > m_defectTable;
		map<string, vector<int > > m_defectTable;
		vector<defect > m_defectList;


};



#endif



