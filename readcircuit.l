/*Scanner for reading benchmark circuit netlist*/

%{
#include "typeemu.h"
#include "readcircuit.tab.h"
//For Win7
//#define	YY_NO_UNISTD_H 1
//#include <io.h>
//End
long lineno=1;
%}

WS [ \t\r]
STRING [_a-zA-z0-9][_a-zA-z0-9\.]*

%option nounput
%option noyywrap
%%
{WS}     { /* Ignore white space*/}
^#[^\n]*\n  { /* Ignore Comment */lineno++; }
^\n {lineno++;}
[\n]  {lineno++; return(EOLINE);}
INPUT {  return(GINPUT);}
OUTPUT {  return(GOUTPUT);}
NOT {  return(GNOT);}
BUF {  return(GBUF);}
AND {  return(GAND);}
OR {  return(GOR);}
NAND {  return(GNAND);}
NOR {  return(GNOR);}
DFF {  return(GDFF);}
TIE0 {  return(GTIE0);}
TIE1 {  return(GTIE1);}
{STRING} { 
        strcpy(yylval.str, (char *)yytext);
        return(NAMESTRING);
      }
"=" { return(EQUAL);}
"(" { return(LPAR);}
")" { return(RPAR);}
"," { return(COMMA);}
.  {printf("Unrecognized symbol error at %s in line %ld\n",yytext, lineno);}
%%

/* debuggin
int main(int argc, char ** argv)
{
        ++argv, --argc;
        if(argc>0)
            yyin = fopen(argv[0],"r");
        else
            yyin=stdin;
        while(!feof(yyin)){ 
            yylex();
        }
        fclose(yyin);
}
*/
