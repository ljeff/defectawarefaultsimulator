/* stuck-at fault simulator for combinational circuit
 * Last update: 2006/12/09 */
#include <iostream>
#include "circuit.h"
#include "GetLongOpt.h"

using namespace std;

extern GetLongOpt option;

// fault simulation test patterns
void CIRCUIT::defectAwareSim(stdCellMap *cellMap)
{
    cout << "Run stuck-at fault simulation" << endl;
    unsigned pattern_num(0);
	cout<<"All possible defect:"<<cellMap->allDefectNum()<<endl;
    if(!Pattern.eof()){ // Readin the first vector
        while(!Pattern.eof()){
            ++pattern_num;
            Pattern.ReadNextPattern();
			resetFaultList();
            //fault-free simulation
            SchedulePI();
            LogicSim();
            //single pattern parallel fault simulation
            FaultSim();
			setDefectDetected(cellMap);;
			cout<<countDetectedDefect(cellMap)<<endl;
        }
    }

    //compute fault coverage
    unsigned total_num(0);
    unsigned undetected_num(0), detected_num(0);
    unsigned eqv_undetected_num(0), eqv_detected_num(0);
    FAULT* fptr;
    list<FAULT*>::iterator fite;
    for (fite = Flist.begin();fite!=Flist.end();++fite) {
        fptr = *fite;
        switch (fptr->GetStatus()) {
            case DETECTED:
                ++eqv_detected_num;
                detected_num += fptr->GetEqvFaultNum();
                break;
            default:
                ++eqv_undetected_num;
                undetected_num += fptr->GetEqvFaultNum();
                break;
        }
    }
    total_num = detected_num + undetected_num;
    cout.setf(ios::fixed);
    cout.precision(2);
    cout << "---------------------------------------" << endl;
    cout << "Test pattern number = " << pattern_num << endl;
    cout << "---------------------------------------" << endl;
    cout << "Total fault number = " << total_num << endl;
    cout << "Detected fault number = " << detected_num << endl;
    cout << "Undetected fault number = " << undetected_num << endl;
    cout << "---------------------------------------" << endl;
    cout << "Equivalent fault number = " << Flist.size() << endl;
    cout << "Equivalent detected fault number = " << eqv_detected_num << endl; 
    cout << "Equivalent undetected fault number = " << eqv_undetected_num << endl; 
    cout << "---------------------------------------" << endl;
    cout << "Fault Coverge = " << 100*detected_num/double(total_num) << "%" << endl;
    cout << "Equivalent FC = " << 100*eqv_detected_num/double(Flist.size()) << "%" << endl;
    cout << "---------------------------------------" << endl;
    return;
}

void CIRCUIT::resetFaultList(){
	FAULT* fptr;
	list<FAULT*>::iterator fite;
	for(fite = Flist.begin();fite != Flist.end();++fite){
		fptr = *fite;
		fptr->SetStatus(UNKNOWN);
	}
}

void CIRCUIT::setDefectDetected(stdCellMap *cellMap){
    FAULT* fptr;
    list<FAULT*>::iterator fite;
    for (fite = Flist.begin();fite!=Flist.end();++fite) {
        fptr = *fite;

        switch (fptr->GetStatus()) {
            case DETECTED:
				cellMap->setDefectDetected(
                	fptr->GetInputGate()->GetName());
                break;
            default:
                break;
        }
    }


}

int CIRCUIT::countDetectedDefect(stdCellMap *cellMap){
	return cellMap->countDetectedDefect();
}
