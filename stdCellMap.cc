#include "stdCellMap.h"

stdCellMap::stdCellMap(string _mapFile){
	m_filePath = _mapFile;
}

void stdCellMap::parse(){
	char line[bufSize];
	PHASE_STD_CELL_MAP phase;
	fstream fp;
	fp.open(m_filePath.c_str(),ios::in);
	if(!fp){
		cout<<"Fail to open the map file "<<m_filePath<<" abort!"<<endl;
		exit(1);
	}

	try{
		stdCell *cell;
		while(fp.getline(line,sizeof(line),'\n')){
			if(strstr(line,"@") != NULL) phase = MODULE_START;
			else if(strstr(line,"input") != NULL) phase = STD_INPUT;
			else if(strstr(line,"output") != NULL) phase = STD_OUTPUT;
			else throw expObj("Undefine map syntax, exit");

			switch(phase){
				case MODULE_START:
					cell = new stdCell(line);
					break;
				case STD_INPUT:
					cell->parseInput(line);
					break;
				case STD_OUTPUT:
					cell->parseOutput(line);
					break;
				default:
					throw expObj("Undefine syntax in stdCellMap file, exit");

			}
			m_stdCellMap.insert(UDT_STR_CELL_MAP::value_type(cell->getName(),cell));

		}

	}catch(expObj e){
		cout<<e.what()<<endl;
		exit(1);
	}
}

/*
void stdCellMap::connectToCircuit(CIRCUIT &circuit){
	UDT_STR_CELL_MAP::iterator it;
	try{
		for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){
			it->second->connectInput(circuit);
			it->second->connectOutput(circuit);
		}
	}catch(expObj e){
		cout<<e.what()<<endl;
	}
}
*/
void stdCellMap::connectDB(defectDB *db){
	cout<<"Connecting to defectDB...";
	defectTable *table;
	UDT_STR_CELL_MAP::iterator it;
	for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){
		table = db->getDefectTableByName(it->second->getType());
		if(table == NULL){
			string err;
			err = "Cant find stdCell " + it->second->getType();
			throw expObj(err.c_str());
		}
		it->second->setDefectTable(table);
	}

	cout<<"done"<<endl;

}

/*
void stdCellMap::setRealPO(CIRCUIT &circuit){
	UDT_STR_CELL_MAP::iterator it;
	for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){
		for(int i = 0;i < it->second->outputNum();i++){
			circuit.addRealPO(it->second->getOutput(i));
		}
	}


}
*/


void stdCellMap::getStdCellVector(vector<stdCell*> &vec){
	UDT_STR_CELL_MAP::iterator it;
	for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){
		vec.push_back(it->second);

	}
}


int stdCellMap::allDefectNum(){
	int num = 0;
	UDT_STR_CELL_MAP::iterator it;
	for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){
		num += it->second->allDefectNum();
	}
	return num;
}


void stdCellMap::setDefectDetected(string name){
	getCellByOutputName(name)->setDefectDetected();

}

stdCell* stdCellMap::getCellByName(string name){
  cout<<name<<endl;
	UDT_STR_CELL_MAP::iterator it;
	it = m_stdCellMap.find(name);
	if(it == m_stdCellMap.end()){
		throw expObj("Cell not exist");
		return NULL;
	}
	cout<<it->second->getName()<<endl;
	return it->second;
}

stdCell* stdCellMap::getCellByOutputName(string name){
    UDT_STR_CELL_MAP::iterator it;                                                        
    for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){                         
		if(it->second->getOutputName(0) == name) return it->second;
        
    }   
	throw expObj("Cell not exist");
	return NULL;

}

int stdCellMap::countDetectedDefect(){
    int num = 0;                                                                          
    UDT_STR_CELL_MAP::iterator it;                                                        
    for(it = m_stdCellMap.begin();it != m_stdCellMap.end();it++){                         
        num += it->second->countDetectedDefect();
    }                                                                                     
    return num;

}


